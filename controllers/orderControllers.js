const Order = require("../models/Order");


//Add Item to Cart
module.exports.addToCart = (reqBody) => {
    let newOrder = new Order({
        user: reqBody.user,
        orderItems: reqBody.orderItems,
        shippingAddress: reqBody.shippingAddress
        })

    return newOrder.save().then((order, error) => {
        if(error){
            return false;
        }
        else{
            console.log(order);
            return true;
        }
    })
    
}


//Get my Orders
module.exports.getOrderById = (params) => {
    
    //look for matching filter
    return Order.findById(params).then( (result, error) => {
        
        if(result == null){
            return `Order not existing`
        } else {
            //return matching document
            if(result){
                return result
            } else {
                //return error
                return error
            }
        }
    })  
}

//Get all orders
module.exports.getAllOrders = (data) => {
    if(data.isAdmin){
            return Order.find().then( (result, error) => {
            if(result){
            return result
             } else {
            return error}
          })

            } else {
                return false
            }

    //return Order.find({}).then(result => result)
    
}