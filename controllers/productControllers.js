const Product = require("../models/Product");


//Create product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		countInStock: reqBody.countInStock
	})
	return newProduct.save().then((product, error) => {
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

//Get all active products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result =>result);
}

//Get specific product
module.exports.getProductById = (params) => {
	
	//look for matching filter
	return Product.findById(params).then( (result, error) => {
		
		//if matching document not found, return Product not existing
		if(result == null){
			return `Product not existing`
		} else {
			//return matching document
			if(result){
				return result
			} else {
				//return error
				return error
			}
		}
	})	
}

//update product info
module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		countInStock: reqBody.countInStock
	}

	//Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//archive product
module.exports.archiveProduct = (productId, reqBody) => {
	let archivedProduct = {
		isActive: false
	}


	return Product.findByIdAndUpdate(productId, archivedProduct).then((isActive, error) => {
		//Product is not archived
		if(error){
			return false;
		}
		//Product is archived
		else{
			return true;
		}
	})
}