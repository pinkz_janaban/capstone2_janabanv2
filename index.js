//Require modules
const express = require("express");
const mongoose = require("mongoose");
//const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

//Create server
const app = express();

//Port
const port = 3001;

//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:kwo3s@cluster0.o5gie.mongodb.net/e-com-app?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."))

//Middlewares
//Allo all resources to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended: true}));
//app.use(cors());


//Routes for our API
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

//Listening to port
//This syntax will allow flexibility when using the application locally or as a hosted application (online).
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})