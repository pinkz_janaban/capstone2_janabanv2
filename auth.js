const jwt = require("jsonwebtoken");

const secret = "crushAkoNgCrushKo";

module.exports.createAccessToken = (user) => {
	//When the user logs in, in a token will be created with the user's information.
	//This will be used for the token payload

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	// Token creation for the session
	//Syntax: jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions]);

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	//get the token in the headers authorization
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		
		token = token.slice(7, token.length)

		//jwt.verify(token, secret, cb(error, data))
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send( {auth: "failed"} )
			} else {
				next()
			}
		})
	}
}

//Middleware functions
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	console.log(typeof token);

	if(typeof token !== "undefined"){
		console.log(token);
		//This removes the "Bearer" 
		token = token.slice(7, token.length);
		console.log(token)

		//Syntax: jwt.verify(token, secretCode, [options/callBackFunction])
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{
				//The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function.
				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}

}

module.exports.decode = (token) =>{
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		//Syntax: jwt.verify(token, secretCode, [options/callBackFunction])
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				//Syntax: jwt.decode(token, [options])
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}
