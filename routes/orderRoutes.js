const express = require("express");
const router = express.Router();

const Order = require("../models/Order");

const orderControllers = require("../controllers/orderControllers")

const auth = require("../auth");


//Add Item to Cart
router.post("/addToCart", (req, res) => {
    orderControllers.addToCart(req.body).then(resultFromController => res.send(resultFromController));
})


//Get all orders
router.get("/allOrders", auth.verify, (req, res) => {
 let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin}    
    
    orderControllers.getAllOrders(data).then(resultFromController => res.send(resultFromController))
    
})


//Get my Orders
router.get("/:orderId", auth.verify, (req, res) => {

    let paramsId = req.params.orderId
    orderControllers.getOrderById(paramsId).then(resultFromController => res.send(resultFromController))
})




module.exports = router;